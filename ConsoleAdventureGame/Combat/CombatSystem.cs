﻿using ConsoleAdventureGame.Entity;
using ConsoleAdventureGame.Map;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAdventureGame.Combat
{
    public class CombatSystem
    {
        private static readonly Random rnd = new Random();

        private static int Hit(int dmg)
        {
            lock (rnd)
            {
                return rnd.Next(0, dmg);
            }
        }

        private static bool CriticalHit()
        {
            int range = 10;

            lock (rnd)
            {
                if (rnd.Next(1, range) < 3) return true;              
                else return false;
            }
        }

        private static bool Dodge()
        {
            int range = 10;

            lock (rnd)
            {
                if (rnd.Next(1, range) < 3) return true;
                else return false;
            }
        }

        private static int GetRandomExp()
        {
            lock (rnd)
            {
                return rnd.Next(30, 60);
            }
        }

        public static void Fight()
        {
            bool status = true;

            while (status)
            {
                //Player Hit the Enemy
                if (!Dodge())
                {
                    int damage = Hit(Player.AttackDamage);

                    //Player's attack will be critical hit or not
                    if (CriticalHit())
                    {
                        Enemy.HP -= damage * 3;
                        WriteCombatSuccess(Player.Name + " hit the enemy by " + damage * 3);
                    }
                    else
                    {
                        Enemy.HP -= damage;
                        WriteCombatSuccess(Player.Name + " hit the enemy by " + damage);
                    }
                }
                else
                {
                    WriteCombatWarning(Enemy.Name + " dodged your hit");
                }

                if (Enemy.HP <= 0)
                {
                    WriteCombatSuccess("Enemy has defeated!");
                    status = false;
                    Player.AddExp(LevelBuilder.Level * GetRandomExp());
                }
                else
                {
                    //Enemy Hit the player
                    if (!Dodge())
                    {
                        int damage = Hit(Enemy.AttackDamage);

                        //Enemy's attack will be critical hit or not
                        if (CriticalHit())
                        {
                            Player.HealthPoint -= damage * 3;
                            WriteCombatWarning(Enemy.Name + " hit you by " + damage * 3);
                        }
                        else
                        {
                            Player.HealthPoint -= damage;
                            WriteCombatWarning(Enemy.Name + " hit you by " + damage);
                        }
                    }
                    else
                    {
                        WriteCombatSuccess("You dodged enemy's attack!");
                    }

                    if (Player.HealthPoint <= 0)
                    {
                        WriteCombatFail("You die!");
                        status = false;
                        Console.ReadKey();
                    }
                }
            }

            Enemy.Reset();
        }

        private static void WriteCombatSuccess(string text)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(text);
            Console.ForegroundColor = ConsoleColor.White;
        }

        private static void WriteCombatWarning(string text)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(text);
            Console.ForegroundColor = ConsoleColor.White;
        }

        private static void WriteCombatFail(string text)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(text);
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
