﻿using ConsoleAdventureGame.Entity;
using ConsoleAdventureGame.Map;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAdventureGame
{
    class Program
    {
        

        static void Main(string[] args)
        {
            Console.WriteLine("Welcome!");
            Console.Write("Choose your name:\t");
            Player.Name = Console.ReadLine();
            Console.WriteLine("Let's Begin!");

            while (true)
            {
                Console.WriteLine("Level - " + Map.LevelBuilder.Level);
                Map.LevelBuilder.BuildMap();
                for (int move = 0; move < Map.LevelBuilder.Map.Length; move++)
                {
                    Event.Actions.Move();
                    Event.Actions.CheckPosition(Map.LevelBuilder.Map[move]);
                }
            }
        }
    }
}
