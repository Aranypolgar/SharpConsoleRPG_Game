﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAdventureGame.Entity
{
    public class Enemy
    {

        public static string Name { get; set; } = (IsBoss) ? "Boss" : "Ambush";

        private static int Level => Map.LevelBuilder.Level;

        private const int BASE_ATTACK_DAMAGE = 5;

        public static int AttackDamage => (IsBoss) ? 
            Level * BASE_ATTACK_DAMAGE * 3 : Level * BASE_ATTACK_DAMAGE;

        private const int BASE_HEALTH_POINT = 25;

        public static int HP = (IsBoss) ? 
            Level * BASE_HEALTH_POINT * 3 : Level * BASE_HEALTH_POINT;

        public static bool IsBoss { get; set; }

        public static void Reset()
        {
            HP = (IsBoss) ?
            Level * BASE_HEALTH_POINT * 3 : Level * BASE_HEALTH_POINT;
        }


    }
}
