﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAdventureGame.Event
{
    public class Actions
    {
        public static void Move()
        {
            bool thinking = true;
            bool heal = true;

            do
            {
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine("What you want?");
                Console.ForegroundColor = ConsoleColor.White;

                if (heal)
                    Console.WriteLine("1 - Sleep and regenerating your hp");

                Console.WriteLine("2 - Move forward");
                Console.WriteLine("3 - Your stats");
                Console.Write("Choose one:\t");

                int input = int.Parse(Console.ReadLine());

                if (heal)
                {
                    if (input == 1)
                    {
                        if (!Player.FullHealthPoint.Equals(Player.HealthPoint))
                        {
                            Player.HealthPoint += 25;

                            if (Player.FullHealthPoint > Player.HealthPoint)
                            {
                                Player.HealthPoint -= (Player.FullHealthPoint - Player.HealthPoint);
                            }
                        }
                        else
                        {
                            Console.ForegroundColor = ConsoleColor.Cyan;
                            Console.WriteLine("Your hp on max");
                            Console.ForegroundColor = ConsoleColor.White;
                        }

                        heal = false;
                    }
                }

                if (input == 2) thinking = false;

                if (input == 3)
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("Your HP:\t" + Player.HealthPoint);
                    Console.WriteLine("Your Attack Damage:\t" + Player.AttackDamage);
                    Console.WriteLine("Your Level:\t" + Player.Level);
                    Console.ForegroundColor = ConsoleColor.White;
                }

            } while (thinking);
   
        }

        public static void CheckPosition(int value)
        {
            if (value == 1)
            {
                Entity.Enemy.IsBoss = false;
                Combat.CombatSystem.Fight();
            }
            if (value == 2)
            {
                Entity.Enemy.IsBoss = true;
                Combat.CombatSystem.Fight();
                Map.Hall.EnterHall();

            }
            if(value == 0)
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("There's nothing...");
                Console.ForegroundColor = ConsoleColor.White;
            }
        }
    }
}
